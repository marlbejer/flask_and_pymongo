from pymongo import MongoClient
from pymongo.errors import ConnectionFailure
from bson.objectid import ObjectId
import json

client = MongoClient('mongodb://localhost:27017')

try:
    client.admin.command('ismaster')
except ConnectionFailure:
    json.dumps({'error': 'Server is Down!'})

db = client['heroes']
heroes = db['heroes']

def add_hero(name):
    result = heroes.insert_one({'name': name})
    added_hero = heroes.find_one(result.inserted_id)
    hero = {}
    hero['name'] = added_hero['name']
    hero['id'] = str(added_hero['_id'])
    return hero

def get_all_heroes():
    result = heroes.find()
    temp_heroes = []
    for r in result:
        hero = {}
        hero['name'] = r['name']
        hero['id'] = str(r['_id'])
        temp_heroes.append(hero)
    return temp_heroes

def get_one_hero(id):
    result = heroes.find_one({'_id': ObjectId(id)})
    hero = {}
    hero['name'] = result['name']
    hero['id'] = str(result['_id'])
    return hero


def get_matched_hero(name):    
    query = {'name': {'$regex': '^'+name}}
    print(query)
    result = heroes.find(query)
    temp_heroes = []
    for r in result:
        print(r)
        hero = {}
        hero['name'] = r['name']
        hero['id'] = str(r['_id'])
        temp_heroes.append(hero)
    return temp_heroes


def update_hero(id, name):
    old_hero = {'_id': ObjectId(id)}
    updated_hero = {'$set': {'name' : name}}
    result = heroes.update_one(old_hero, updated_hero)
    return get_one_hero(id)


def delete_hero(id):
    query = {'_id': ObjectId(id)}
    heroes.delete_one(query)
    return 'Deleted'