FLASK API using PyMongo

This is an skeletal/backbone of a basic API using HTTP verbs(GET, POST, PUT, DELETE). PyMongo is for MongoDb for Python.

- run the Virtual Environment
    * run using CMD
        go to env/Scripts and type 'activate.bat' to run

    * run using Git bash
        go to env/Scripts and type '. activate' to run

- install dependencies
    type 'pip install -r requirements.txt'

- run the application
    * run using python command / CMD
        type 'python app.python'
        or
        type 'set FLASK_APP=app.py' then 'set FLASK_ENV=development' lastly 'flask run'

- tested the API using -> Postman