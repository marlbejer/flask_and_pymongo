from flask import Flask, jsonify, request
import pymongodb as db

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello, World!'

@app.route('/heroes', methods=['GET', 'POST', 'DELETE'])
def heroes():
    if request.method == 'POST':
        hero = request.json['name']
        response = db.add_hero(hero)   
        return jsonify({'result': response})
    
    else:
        response = db.get_all_heroes()   
        return jsonify({'result': response}) 


@app.route('/details/<string:id>', methods=['GET', 'PUT'])
def details(id):
    if request.method == 'GET':
        response = db.get_one_hero(id)
        return jsonify({'result': response})

    else:
        name = request.json['name']
        response = db.update_hero(id, name)
        return jsonify({'result': response})

@app.route('/delete/<string:id>', methods=['DELETE'])
def delete(id):
    response = db.delete_hero(id)
    return jsonify({'result': response})


@app.route('/dashboard/<string:name>', methods=['GET'])
def get_matched(name):
    response = db.get_matched_hero(name)
    return jsonify({'result': response})

if __name__ == '__main___':
    app.run(debug=True)